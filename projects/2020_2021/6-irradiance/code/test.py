# -*- coding: utf-8 -*-
"""
Created on Thu May  7 11:57:40 2020

@author: comed
"""


from imageio import *
import matplotlib.pyplot as plt
import numpy as np
from math import *




def moy(triplet):
    r,v,b=triplet[0],triplet[1],triplet[2]
    M=sqrt ((r**2+v**2+b**2)/3)
    return M



def pluie(k):
    A=imread('pluie_nao_small.png')
    jmax=len(A[0])
    imax=len(A)
    ci=imax//2
    cj=jmax//2 
    for i in range (1,imax-1):
        for j in range (1,jmax-1):
            d = sqrt((i-ci)**2+(j-cj)**2)
            if d< (100):
                if k==0 :
                    A[i][j][0]=int(0.5*(A[i-1][j-1][0]+A[i-1][j][0]+A[i-1][j+1][0]+A[i][j-1][0]+A[i][j+1][0]+A[i+1][j-1][0]+A[i+1][j][0]+A[i+1][j+1][0]))
                    A[i][j][1]=int(0.5*(A[i-1][j-1][1]+A[i-1][j][1]+A[i-1][j+1][1]+A[i][j-1][1]+A[i][j+1][1]+A[i+1][j-1][1]+A[i+1][j][1]+A[i+1][j+1][1]))
                    A[i][j][2]=int(0.5*(A[i-1][j-1][2]+A[i-1][j][2]+A[i-1][j+1][2]+A[i][j-1][2]+A[i][j+1][2]+A[i+1][j-1][2]+A[i+1][j][2]+A[i+1][j+1][2]))

    

def rgb_to_gray():
    A=imread('cam2 UTC 19-05-06_08-59-59-57_small.jpg')
    jmax=len(A[0])
    imax=len(A)
    
    B=A
    print(B.shape)
    g=0.
    for i in range(imax):
        for j in range(jmax):
            r=B[i][j][0]
            v=B[i][j][1]
            b=B[i][j][2]
            g=float ((b-r)/(b+r+1)) + 1.
            g= g * 255/2
            g= g//1
            print(g)
            B[i][j][0]=g
            B[i][j][1]=g
            B[i][j][2]=g
        print(i)

    plt.imshow(B)


def rayon_disque(k): #donne le rayon en nbr de pixels depuis un nbr de pixels
    #s=pi()*r*r donc r=sqrt(s/pi)
    return int(sqrt(k/np.pi))


    
    
def a (k):
    if k==1 : A=imread('cam2 UTC 19-05-06_08-59-59-57_small.jpg')
    if k==2 : A=imread('echantillon_30_07_small.jpg')
    if k==3 : A=imread('juillet (114)_small.jpg')
    if k==4 : A=imread('nuage laiteux_small.jpg')
    if k==5 : A=imread('nuit_nuage_small.jpg')
    if k==6 : A=imread('nuit_claire_small.jpg')
    if k==7 : A=imread('soleil_derriere_nuage_clair_small.jpg')
    if k==8 : A=imread('juillet (196)_small.jpg')
    if k==9 : A=imread('soleil_deforme_small.jpg')
    plt.imshow(A)

def b(k):
    if k==1 : A=imread('cam2 UTC 19-05-06_08-59-59-57_small.jpg')
    if k==2 : A=imread('echantillon_30_07_small.jpg')
    if k==3 : A=imread('juillet (114)_small.jpg')
    if k==4 : A=imread('nuage laiteux_small.jpg')
    if k==5 : A=imread('nuit_nuage_small.jpg')
    if k==6 : A=imread('nuit_claire_small.jpg')
    if k==7 : A=imread('soleil_derriere_nuage_clair_small.jpg')
    if k==8 : A=imread('juillet (196)_small.jpg')
    if k==9 : A,heure=imread('soleil_deforme_small.jpg'),5
    heure = 5 
    B=A
    marge_eclairement_soleil=50
    marge_soleil=1.5
    
    jmax=len(A[0])
    imax=len(A)
    print('L\'image fait',imax,'pixels de haut et',jmax,'de long.')
    nbr_pixels_soleil=0
    MAX=0
    isoleil=0
    jsoleil=0
    pixels_soleil=[]
    #on cache le logo E3:
    for i in range(40):
        for j in range (120):
            A[i][j]= [0,0,0]
    
    #recherche lum soleil:
    for i in range (imax):
        for j in range (jmax):
            n=moy(A[i,j])
            if n>MAX :
                MAX=n
    print('L\'éclairement max est de',MAX)
    
    
    #recherche du soleil
    for i in range (imax):
        for j in range (jmax):
            N=moy(A[i][j])
            if N>MAX-marge_eclairement_soleil:
                isoleil+=i
                jsoleil+=j
                nbr_pixels_soleil+=1
                pixels_soleil+=[(i,j)]
   # print('les coords i du soleil sont',ISOLEIL,'\n\net les coords en j sont',JSOLEIL)
    
    jsoleil=int(jsoleil/nbr_pixels_soleil)
    isoleil=int(isoleil/nbr_pixels_soleil)
    print('Le soleil est composé de ', nbr_pixels_soleil , 'pixels.')
    print('et ses coords sont',isoleil,jsoleil)
    A[isoleil][jsoleil]=[255,0,0]

    # nuages : rouge/bleu 
    #1.5 12.9 soit largeur soleil=1.5*577/12.9=67.1
    largeur_soleil=int(2*rayon_disque(nbr_pixels_soleil)*marge_soleil)
    
    #correction du soleil
    if heure > 12 :couleur_bord = A[isoleil-int((largeur_soleil//2)//1.414)][jsoleil-int((largeur_soleil//2)//1.414)]
    if heure <= 12: couleur_bord =A[isoleil+int((largeur_soleil//2))][jsoleil+int((largeur_soleil//2))]
    print('La couleur du bord est',couleur_bord)
    
    
    for a in range (nbr_pixels_soleil):
        i,j=pixels_soleil[a][0],pixels_soleil[a][1]
        A[i,j]=couleur_bord
    
    
    plt.imshow(A)
    #for i in range (isoleil-largeur_soleil,isoleil+largeur_soleil):
     #   for j in range(jsoleil-largeur_soleil,jsoleil+largeur_soleil):    #on évite de tout reparcourir
      #      if sqrt((i-isoleil)**2+(j-jsoleil)**2) < largeur_soleil/2:
       #         if 0<i<imax and 0<j<jmax :
        #            A[i][j]=couleur_bord


    
                
    
                
